;;; org-bullets-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "org-bullets" "org-bullets.el" (0 0 0 0))
;;; Generated autoloads from org-bullets.el

(autoload 'org-bullets-mode "org-bullets" "\
Use UTF8 bullets in Org mode headings.

\(fn &optional ARG)" t nil)

;;;***

(provide 'org-bullets-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; org-bullets-autoloads.el ends here
