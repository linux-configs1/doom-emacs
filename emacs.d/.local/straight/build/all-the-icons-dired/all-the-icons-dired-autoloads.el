;;; all-the-icons-dired-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "all-the-icons-dired" "all-the-icons-dired.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from all-the-icons-dired.el

(autoload 'all-the-icons-dired-mode "all-the-icons-dired" "\
Display all-the-icons icon for each files in a dired buffer.

\(fn &optional ARG)" t nil)

;;;***

(provide 'all-the-icons-dired-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; all-the-icons-dired-autoloads.el ends here
