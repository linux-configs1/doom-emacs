;;; -*- no-byte-compile: t -*-
(define-package "emacsql-sqlite3" "20200906.1605" "Yet another EmacSQL backend for SQLite" '((emacs "26.1") (emacsql "3.0.0")) :commit "de5b29894553cd77e99da6f857b743219fd935a2" :keywords '("extensions") :authors '(("Zhu Zihao" . "all_but_last@163.com")) :maintainer '("Zhu Zihao" . "all_but_last@163.com") :url "https://github.com/cireu/emacsql-sqlite3")
